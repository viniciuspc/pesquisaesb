package br.com.fiap.esb.web;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ClienteVO implements Serializable {
	
	private static final long serialVersionUID = -5429932005321639112L;
	
	private String nome;
	private String telefone;
	private Date dataNascimento;
	private String dataNascimentoStr;
	private String cep;
	private String endereco;
	private String bairro;
	private String cidade;
	
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
		
		Date dt = dataNascimento;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		setDataNascimentoStr(sdf.format(dt));
		
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getDataNascimentoStr() {
		return dataNascimentoStr;
	}
	public void setDataNascimentoStr(String dataNascimentoStr) {
		this.dataNascimentoStr = dataNascimentoStr;
	}
	
}
