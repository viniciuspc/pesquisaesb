package br.com.fiap.esb.web.bean;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.bean.ManagedBean;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.WriterOutputStream;

import br.com.fiap.esb.web.ClienteVO;
import br.com.fiap.esb.web.EnderecoCorreios;

import com.google.gson.Gson;

@ManagedBean(name="cadastroCliente")
public class CadastroClienteBean {
	
	private ClienteVO clienteVO = new ClienteVO();
	

	public ClienteVO getClienteVO() {
		return clienteVO;
	}

	public void setClienteVO(ClienteVO clienteVO) {
		this.clienteVO = clienteVO;
		
	}
	
	public String atualizaCep(){
		
		Gson gson = new Gson();
		
		//gson.toJson(clienteVO);
		
		try{
			URL url = new URL("http://localhost:8084/buscaCep?cep="+clienteVO.getCep());
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			
			
			/*System.out.println(params);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(params);
			wr.flush();
			wr.close();*/
			
			InputStream input = con.getInputStream();
			
			StringWriter sw = new StringWriter();
			IOUtils.copy(input, sw);
			
			EnderecoCorreios endereco = gson.fromJson(sw.toString(),EnderecoCorreios.class);
			clienteVO.setBairro(endereco.getBairro());
			clienteVO.setCidade(endereco.getCidade());
			clienteVO.setEndereco(endereco.getTipo_logradouro()+" "+endereco.getLogradouro());
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		
		/*try {
			
			//TODO Substituir para receber do ESB
			URL url = new URL("http://cep.republicavirtual.com.br/web_cep.php?cep="+ clienteVO.getCep() + "&formato=json");

			InputStream is = (InputStream) url.openConnection().getContent();
			StringWriter sw = new StringWriter();
			IOUtils.copy(is, sw);

			Gson gson = new Gson();
			EnderecoCorreios endereco = gson.fromJson(sw.toString(),EnderecoCorreios.class);
			clienteVO.setBairro(endereco.getBairro());
			clienteVO.setCidade(endereco.getCidade());
			clienteVO.setEndereco(endereco.getTipo_logradouro()+" "+endereco.getLogradouro());

		} catch (Exception e) {
			e.printStackTrace();
		}*/
		
		return "";
	}
	
	

	/*public String cadastrar() {
		
		Gson gson = new Gson();
		String clienteJson = gson.toJson(clienteVO);
		
		String name = "cliente_"+ (new Date()).getTime();
		File path = new File("/home/carlos/fiap/cliente_json/"+name+".json");
		
		try {
			BufferedWriter out = new BufferedWriter( new FileWriter(path));
			out.write(clienteJson);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "cadastroCliente.xhtml";
	}*/
	
	public String cadastrar() {
		
		Gson gson = new Gson();
		String clienteJson = gson.toJson(clienteVO);
		
		try{
			URL url = new URL("http://localhost:8084/incluirCliente");
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			con.setDoInput(true);
			con.setDoOutput(true);
			
			
			
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(clienteJson);
			wr.flush();
			wr.close();
			
			
			InputStream input = con.getInputStream();
			
			StringWriter sw = new StringWriter();
			IOUtils.copy(input, sw);
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return "cadastroCliente.xhtml";
	}

}
