package br.com.fiap.esb.web.bean;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.apache.commons.io.input.ReaderInputStream;

import com.google.gson.Gson;

import br.com.fiap.esb.web.ClienteVO;

@ManagedBean(name="listaCliente")
public class ListaClienteBean {
	
	/*public List<ClienteVO> getListaClientes(){
		
		//TODO Substituir pelo oque estiver vindo do ESB
		String[] listaClientesGson = new String[3];
		listaClientesGson[0] = "{\"nome\":\"Vinicius P Carvalho\",\"telefone\":\"988660182\",\"dataNascimento\":\"Feb 11, 1991 12:00:00 AM\",\"cep\":\"03150010\",\"endereco\":\"Rua coelho neto\",\"bairro\":\"Quinta da Paineira\",\"cidade\":\"S�o Paulo\"}";
		listaClientesGson[1] = "{\"nome\":\"Teste 1\",\"telefone\":\"988660182\",\"dataNascimento\":\"Feb 11, 1991 12:00:00 AM\",\"cep\":\"03150010\",\"endereco\":\"Rua Maria Daffre\",\"bairro\":\"Quinta da Paineira\",\"cidade\":\"S�o Paulo\"}";
		listaClientesGson[2] = "{\"nome\":\"Leonardo P Carvalho\",\"telefone\":\"988884547\",\"dataNascimento\":\"May 26, 1994 12:00:00 AM\",\"cep\":\"05508000\",\"endereco\":\"Avenida Professor Lineu Prestes\",\"bairro\":\"Cidade Universit�ria\",\"cidade\":\"S�o Paulo\"}";
		
		Gson gson = new Gson();
		
		List<ClienteVO> lsitaClientesVO = new ArrayList<ClienteVO>();
		for (String clienteGson : listaClientesGson) {
			lsitaClientesVO.add(gson.fromJson(clienteGson, ClienteVO.class));
		}
		
		
		return lsitaClientesVO;
	}*/
	
	
	public List<ClienteVO> getListaClientes(){
		Gson gson = new Gson();
		BufferedReader input = null;
		
		File path = new File("/home/carlos/fiap/cliente_list_json/clientes.json");
		StringBuilder jsonStr = new StringBuilder();
		try {
			input = new BufferedReader(new FileReader(path));
			
			String line = null;
			while( (line = input.readLine() ) != null ){
				jsonStr.append(line);
			}
			input.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<ClienteVO> clientes = null;
		
		clientes = gson.fromJson(jsonStr.toString(), List.class);
		
		return clientes;
	}

}
