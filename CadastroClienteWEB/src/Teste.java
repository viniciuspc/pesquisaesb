import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;

import org.apache.commons.io.IOUtils;

import br.com.fiap.esb.web.EnderecoCorreios;

import com.google.gson.Gson;


public class Teste {
	
	public static void main(String[] args) throws IOException {
		URL url = new URL("http://cep.republicavirtual.com.br/web_cep.php?cep=05508000&formato=json");
		
		InputStream is = (InputStream) url.openConnection().getContent();
		StringWriter sw = new StringWriter();
		IOUtils.copy(is, sw);
		System.out.println(sw);
		
		Gson gson = new Gson();
		EnderecoCorreios cep = gson.fromJson(sw.toString(), EnderecoCorreios.class);
		
	}

}
