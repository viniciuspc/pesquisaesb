
package br.com.fiap.main;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.fiap.bean.Cliente;
import br.com.fiap.dao.ClienteDao;

public class CSV {
	
	public static void main(String[] args){
		
		CSV cliente = new CSV();
		cliente.createCsvFile();
		
	}
	
	public void createCsvFile(){
		
		try{
			BufferedWriter bw = new BufferedWriter(new FileWriter("/home/carlos/fiap/cliente_list/cliente.csv"));
			
			List<Cliente> clientes = new ArrayList<>();
			ClienteDao clienteDao = new ClienteDao();
			clientes = clienteDao.consultarCliente();	
								
			for(int i = 0; i < clientes.size(); i++){
				
				bw.write(clientes.get(i).getNome()    +";"+
			             clientes.get(i).getTelefone()+";"+
						 clientes.get(i).getDataNasc()+";"+
			             clientes.get(i).getCidade()  +";"+
			             clientes.get(i).getEndereco()+";"+
			             clientes.get(i).getBairro()  +";"+
			             clientes.get(i).getCep()+"\n");
			}
			
			bw.close();
			
		}catch (FileNotFoundException ex){
		    ex.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace(); 
		}
	}	
}


