package br.com.fiap.dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Dao {
	
	private String url = "jdbc:mysql://localhost:3306/fiap_mvp";
	private String user = "root";
	private String password = "";
	private Connection con;
	
	public Connection conectar(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
		    con = DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {		
			e.printStackTrace();
		}
		return con;
	}
	
	public void desconectar(){
		try{
			con.close();
			
		}catch(SQLException e){
			
		}			
	}

}
