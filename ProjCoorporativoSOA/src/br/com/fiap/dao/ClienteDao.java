package br.com.fiap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.fiap.bean.Cliente;

public class ClienteDao {

	private PreparedStatement st;	
	private ResultSet rs;
	private Connection con;

	public List<Cliente> consultarCliente() {

		List<Cliente> lista = new ArrayList<Cliente>();

		Dao conexao = new Dao();
		Cliente cliente;
		con = conexao.conectar();
		if (con != null) {

			try {
				st = con.prepareStatement("SELECT * FROM cliente");
				rs = st.executeQuery();

				while (rs.next()) {
					cliente = new Cliente();
					cliente.setIdCliente(rs.getInt("id_cliente"));
					cliente.setNome(rs.getString("nome"));
					cliente.setTelefone(rs.getString("telefone"));
					cliente.setDataNasc(rs.getDate("data_nascimento"));
					cliente.setCep(rs.getString("cep"));
					cliente.setCidade(rs.getString("cidade"));
					cliente.setEndereco(rs.getString("endereco"));
					cliente.setBairro(rs.getString("bairro"));
					
					

					lista.add(cliente);
				}
											
			} catch (SQLException e) {				
				e.printStackTrace();
			}		
		}
		return lista;
	}
}
