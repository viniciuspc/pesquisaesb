package br.com.fiap;

import java.io.File;

import br.com.fiap.dao.ClienteDAO;
import br.com.fiap.db.DB;
import br.com.fiap.entity.ClienteEntity;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		File path = new File("/home/carlos/fiap/cliente_xml/cliente.xml");
		System.out.println("Inidiando a aplicação");
		
		while(true){
			
			if(!path.exists()){
				try {
					System.out.println("sleep 1s...");
					Thread.sleep(1000);
					continue;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println("iniciar o cadastro");
			
			ClienteEntity clienteEntity = null;
			XmlToObject x = new XmlToObject(path);
			try{
				clienteEntity = x.parse();
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			DB db = new DB();
			db.open();
			ClienteDAO clienteDAO = new ClienteDAO(db);
			clienteDAO.insert(clienteEntity);
			db.close();
			
			path.delete();
			
			System.out.println("finalizando o cadastro");
		}

	}

}
