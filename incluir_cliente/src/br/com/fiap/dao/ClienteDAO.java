package br.com.fiap.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import br.com.fiap.db.DB;
import br.com.fiap.entity.ClienteEntity;

public class ClienteDAO {
	private DB db;
	
	public ClienteDAO(DB db){
		this.db = db;
	}
	
	public void insert(ClienteEntity clienteEntity){
		Date dateNasc =  new Date(clienteEntity.getDataNasc().getTime());
		
		try {
			PreparedStatement pstm = db.prepareStatement("insert into cliente " +
					"(nome, telefone, data_nascimento, cep, cidade, endereco, bairro) values " +
					"(?, ?, ?, ?, ?, ?, ?)");
			pstm.setString(1, clienteEntity.getNome());
			pstm.setString(2, clienteEntity.getTelefone());
			pstm.setDate(3, dateNasc);
			pstm.setString(4, clienteEntity.getCep());
			pstm.setString(5, clienteEntity.getCidade());
			pstm.setString(6, clienteEntity.getEndereco());
			pstm.setString(7, clienteEntity.getBairro());
			
			pstm.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
