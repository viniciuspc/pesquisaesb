package br.com.fiap;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import br.com.fiap.entity.ClienteEntity;

public class XmlToObject {
	private File file;
	public XmlToObject(File file) {
		this.file = file;
	}
	
	public ClienteEntity parse() throws Exception{
		ClienteEntity clienteEntity = new ClienteEntity();
		
		SAXBuilder sax;
		Document document;
		
		sax = new SAXBuilder();
		document = sax.build(file);
		
		Element eRoot = document.getRootElement();
		
		
		Element elem = eRoot.getChild("node").getChild("__children");
		for(Element e : elem.getChildren()){
			String cName = getChildren(e, "string").getValue();
			String cValue = getChildren(e, "org.codehaus.jackson.node.TextNode").getChild("__value").getValue();
			
			if("nome".equals(cName)){
				clienteEntity.setNome(cValue);
			} else if("telefone".equals(cName)){
				clienteEntity.setTelefone(cValue);
			}  else if("dataNascimentoStr".equals(cName)){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date dt = sdf.parse(cValue);
				clienteEntity.setDataNasc(dt);
			} else if("cep".equals(cName)){
				clienteEntity.setCep(cValue);
			} else if("endereco".equals(cName)){
				clienteEntity.setEndereco(cValue);
			} else if("bairro".equals(cName)){
				clienteEntity.setBairro(cValue);
			} else if("cidade".equals(cName)){
				clienteEntity.setCidade(cValue);
			}
		}
		
		
		
		return clienteEntity;
	}
	
	private Element getChildren(Element element, String key){
		for(Element e : element.getChildren()){
			if(e.getName().equals(key)){
				return e;
			}
		}
		return null;
	}

}
